import React from "react";
import Router from "./src/routing";
import { NavigationContainer } from '@react-navigation/native';


export default function App(params) {
    return (
        <NavigationContainer>
            <Router initialRouteName={"contactsList"}/>
        </NavigationContainer>);
}