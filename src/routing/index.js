import {createStackNavigator} from '@react-navigation/stack';
import React from "react";
import ContactsList from "../components/ContactsList";
import ContactPage from "../components/ContactPage";

const Stack = createStackNavigator();

export default function MyStack() {
    return (
        <Stack.Navigator>
            <Stack.Screen name="ContactsList" component={ContactsList}/>
            <Stack.Screen name="ContactsPage" component={ContactPage}/>
        </Stack.Navigator>
    );
}