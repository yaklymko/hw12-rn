import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View, TextInput} from 'react-native';
import * as Contacts from 'expo-contacts';

export default function ContactsList(props) {
    const [contacts, setContacts] = useState([]);
    const [searchText, setSearchText] = useState("");

    const onChange = (text) => {
        setSearchText(text);
    }

    function filterContacts(contacts, filter){
        return contacts.filter(c=>c.name.includes(filter))
    }


    useEffect(() => {
        (async () => {
            const {status} = await Contacts.requestPermissionsAsync();
            if (status === 'granted') {
                const {data} = await Contacts.getContactsAsync({
                    fields: [Contacts.Fields.PhoneNumbers]
                });

                if (data.length > 0) {
                    setContacts(data.filter(c => c.phoneNumbers))
                }
            }
        })();
    }, []);

    return (
        <View style={styles.mainContainer}>
            <TextInput
                style={styles.search}
                placeholder="Search contacts"
                value={searchText}
                onChangeText={onChange}
            />
            <FlatList
                data={filterContacts(contacts, searchText)}
                renderItem={(arg)=>renderContact(arg, props.navigation)}
                keyExtractor={item => item.id}
            />
        </View>
    );

}

function renderContact(obj, navigation) {
    const contact = obj.item;
    const {name} = contact;

    return (
        <TouchableOpacity onPress={() => navigation.navigate('ContactsPage', contact)}>
            <View style={styles.contactBlock}>
                <Text>{name}</Text>
            </View>
        </TouchableOpacity>

    );
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: '#fff',
    },

    contactBlock: {
        padding: 10,
        marginTop: 15,
        justifyContent: "center",
        flex: 1,
        flexDirection: "row",
        borderWidth: 2,
        borderColor: "grey",
        borderRadius: 50,
    },
    search: {

        borderColor: "grey",
        borderRadius: 3,
        marginBottom: 10,
        padding: 5,
        borderWidth: 1,
    },
});
