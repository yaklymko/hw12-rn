import React from 'react';
import {Button, Linking, StyleSheet, Text, View} from 'react-native';

export default function ContactsPage(props) {

    const contact = props.route.params;
    const name = contact.name || "";
    const number = contact.phoneNumbers[0].number;

    async function deleteContactAsyncMock(id) {
        return "";
    }

    async function onDelete(id) {
        // Doesn't work in case you don't build expo app to binaries
        //const deleteRes = await Contacts.removeContactAsync(id);
        await deleteContactAsyncMock(id);
        props.navigation.goBack();
    }

    async function onCall(number) {
        const url = `tel://${number}`;
        await Linking.openURL(url);
    }

    return (
        <View style={styles.contactCard}>
            <View style={styles.infoList}>
                <View style={[styles.listItem]}>
                    <Text style={[styles.listHeader]}>Name:</Text>
                    <Text>{name}</Text>
                </View>

                <View style={[styles.listItem]}>
                    <Text style={[styles.listHeader]}>Number:</Text>
                    <Text>{number}</Text>
                </View>

                <View style={[styles.listButton]}>
                    <Button title={"Delete"} onPress={() => onDelete(contact.id)} color={"grey"}/>
                </View>

                <View style={[styles.listButton]}>
                    <Button title={"Call"} onPress={() => onCall(number)} color={"green"}/>
                </View>

            </View>
        </View>
    );

}

const styles = StyleSheet.create({
    contactCard: {
        flex: 1,
        alignItems: "center",
        paddingTop: 100
    },
    infoList: {
        flex: 1,
        flexWrap: "wrap",
        alignItems: "flex-start",
    },
    listHeader: {
        textDecorationLine: "underline",
        fontWeight: "bold",
    },

    listItem: {
        textAlign: "left",
        marginVertical: 10
    },

    listButton: {
        width: 200,
        marginVertical: 10
    }
});
